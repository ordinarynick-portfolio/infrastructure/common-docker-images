FROM alpine:3.19

# Set needed variables
ENV JAVA_HOME="/usr/lib/jvm/default-jvm/"
ENV PATH=$PATH:${JAVA_HOME}/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
ENV LANG=en_US.UTF-8 LANGUAGE=en_US:en LC_ALL=en_US.UTF-8

# Install utils and Java
RUN /bin/sh -c set -eux;\
    apk add --no-cache \
    bash=5.2.21-r0 \
    fontconfig=2.14.2-r4 \
    font-dejavu=2.37-r5 \
    java-cacerts=1.0-r1 \
    libretls=3.7.0-r2 \
    zlib=1.3-r2 \
    musl-locales=0.1.0-r1 \
    musl-locales-lang=0.1.0-r1 \
    binutils=2.41-r0 \
    tzdata=2023d-r0 \
    jq=1.7.1-r0 \
    openjdk21-jdk=21.0.1_p12-r0;\
    rm -rf /var/cache/apk/*

# Verify Java instalation
RUN /bin/sh -c set -eux; \
    echo "javac --version"; javac --version;\
    echo "java --version"; java --version;

CMD ["jshell"]
