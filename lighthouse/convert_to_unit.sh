#!/bin/bash
###################################################################
# Script Name	: convert_to_junit.sh
# Description	: Converts Lighthouse assertion json to JUnit xml.
# Args			: #1: Lighthouse assertion JSON file.
# Author		: Ordinary Nick
# Email			: rostislav@ordinarynick.com
###################################################################

#
# Starts testsuite JUnit XML tag.
#
# @param 1 Count of the all testcases in testsuite.
# @param 2 Count of the failed testcases.
#
function start_testsuite() {
  testcase_counts=$1
  testcase_failures_count=$2

  echo '<?xml version="1.0" encoding="UTF-8"?>'
  echo "<testsuite id=\"lighthouse_suite\" name=\"Lighthouse check\" tests=\"$testcase_counts\" failures=\"$testcase_failures_count\">"
}

#
# Ends testsuite JUnit XML tag.
#
function end_testsuite() {
  echo "</testsuite>"
}

#
# Prints JUnit testcase from array.
#
# @param 1 Array with filled JUnit testcase values.
#
function print_junit_testcase() {
  junit_testcase=$1

  echo "  <testcase id=\"${junit_testcase['id']}\" name=\"${junit_testcase['name']}\">"
  echo "    <failure message=\"${junit_testcase['failure_message']}\" type=\"${junit_testcase['failure_type']}\">"
  echo "${junit_testcase['failure_detail_message']}"
  echo "    </failure>"
  echo "  </testcase>"
}

#
# Maps JSON object of Lighthouse report case to JUnit testcase.
#
# @param 1 Lighthouse case JSON object.
#
function map_lighthouse_case_to_junit_testcase() {
  lighthouse_json=$1

  declare -A json
  while IFS="=" read -r key value
  do
      json[$key]="$value"
  done < <(echo "$lighthouse_json" |  jq -r 'to_entries | map("\(.key)=\(.value)") | .[]')

  declare -A junit_testcase
  junit_testcase['id']=${json['auditId']}
  junit_testcase['name']="${junit_testcase['id']} failure for ${json['name']} assertion"
  junit_testcase['failure_type']=${json['level']}
  junit_testcase['failure_message']=${json['auditTitle']}

  operator=$(echo "${json['operator']}" | sed 's/&/\&amp;/g; s/</\&lt;/g; s/>/\&gt;/g')
  junit_testcase['failure_detail_message']="Actual value \\\"${json['actual']}\\\" is not $operator than expected \\\"${json['expected']}\\\"!\nMore information at ${json['auditDocumentationLink']}."

  # TODO Make it as return value instead.
  print_junit_testcase "${junit_testcase[@]}"
}

#
# Map Lighthouse report file to JUnit suite.
#
# @param 1 Lighthouse JSON case in indexed array.
#
# TODO use this function
function map_lighthouse_suite_to_junit_suite() {
  lighthouse_json=$1

  start_testsuite ${#lighthouse_json[@]} ${#lighthouse_json[@]}
  for key in "${!lighthouse_json[@]}"
  do
    map_lighthouse_case_to_junit_testcase "${lighthouse_json[$key]}"
  done
  end_testsuite
}

# Main function
FILE=$1

declare -A json
while IFS="=" read -r key value
do
  json[$key]="$value"
done < <(jq -r 'to_entries | map("\(.key)=\(.value)") | .[]' $FILE)

start_testsuite ${#json[@]} ${#json[@]}
for key in "${!json[@]}"
do
  map_lighthouse_case_to_junit_testcase "${json[$key]}"
done
end_testsuite
