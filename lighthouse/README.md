# Lighthouse CI image
Docker image containing installed Lighthouse CLI tool from NPM repository. It also contains installed Chromium as web browser for use in tests and Lighthouse JSON report converter to Junit XML.

## Installed packages
- Node - 19.3
- Chromium - 108
- Lighthouse CLI - 0.10.0

## Usage
Typical usage in Gitlab CI pipeline is:

``` yaml
# Measure performance of web with sla.
Performance:
    stage: Performance
    image: registry.gitlab.com/ordinarynick-portfolio/common-docker-images/lighthouse:latest
    variables:
    script:
        - lhci autorun --chrome-flags="--no-sandbox"
        - ./lighthouse-xml-to-junit-xml.js -o junit.xml lighthouse-report.json
    artifacts:
        reports:
            junit: junit.xml
    rules:
        - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
```
