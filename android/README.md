# Android
Docker image containing installed Android SDK, OpenJDK and some tools.

## Installed packages
- Android SDK 34
- OpenJDK - 21
- binutils
- jq
