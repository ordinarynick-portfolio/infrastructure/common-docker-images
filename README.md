# Common Docker Images

Contains common docker images used in Gitlab CI/CD pipelines of my projects.

## Docker images

- [**Lighthouse**](lighthouse) - Docker image with installed Lighthouse CLI tool to test website performance.
- [**Docker Java**](docker-java) - Docker image with Docker and intalled Java.
- [**Android**](android) - Docker image with OpenJDK and tools.
- [**Java**](java) - Docker image with installed Android SDK.
